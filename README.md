# F1

Here are all my visualizations concerning Formula 1.

For comments, follow me on [Twitter](https://twitter.com/germandatadude).

# Season 2020

## Race-pace comparison

[Comparison of Race pace between Teammates](https://gitlab.com/germandatadude/f1/-/blob/master/plots/2020-patchwork.png)

![Link: https://gitlab.com/germandatadude/f1/-/raw/master/plots/2020-patchwork.png](https://gitlab.com/germandatadude/f1/-/raw/master/plots/2020-patchwork.png)